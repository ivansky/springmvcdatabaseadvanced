# Practice task

1. Практику по WEB Basics переделать на Spring MVC
2. Написать REST-сервис для основных объектов из практики по JDBC, например, список автомобилей
	1. Использовать за основу https://spring.io/guides/gs/rest-service/ 
	2. Сделать возможность поиска объектов по атрибутом, например, как тут:  https://dev.twitter.com/rest/reference/get/search/tweets
	3. Приложение должно быть потокозащищенным
	4. * Реализовать пэйджинг по аналогии с https://dev.twitter.com/rest/public/timelines, например, https://dev.twitter.com/rest/reference/get/lists/subscribers
